# Master Thesis
## Loïc Guibert · Master MSE in Data Science · 2022-2023

This project contains all the resources related to my master thesis, both administrative and technical.

**Links to resources**
* [Report](report/thesis.pdf)
* [Specifications document](specifications/specifications.pdf)
* [All the Minutes of Meetings](meetings/)
* Poster : [PDF](resources/poster.pdf) / [PPTX](resources/poster.pptx)
* [Zotero Export](review/)

**Links to thesis outcomes**
* [Guide Dataset](data/dataset.ods)
* [Web application](app/)
* [Data Converter](data/converter/)