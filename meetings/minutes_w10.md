# Meeting Minutes

**Week number**: 10


## Meeting Information

**Date/Time:** 23 November 2022, 1.30pm

**Purpose:** Weekly Meeting

**Location:** UBS131 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Discussion about the other evaluation tools | Comments about this topic. |
| 3 | Planning changes | Some differences in the initial planning and activities. |
| 4 | Pascal's visit to Winchester and talks. | Comments about this topic. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | The comparison is almost finished. Once done, the next activity will be launched in a new Chapter, which is the Contribution. |
| 3 | LG | Because of the decision made on the other evaluation tool comparison, some changes had to be done according to the initial planning. |
| 4 | All attendees | Discussion about Pascal's visit to Winchester and talks, he has to provide an abstract on the topics he will talk about. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 2 | LG | This week | Finish the Comparison chapter. |
| 2 | LG | This week | Start the Contribution chapter. |
| 4 | PB | This week | Send the talk abstract to AW. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Wednesday, at room UBS131 on 1.30pm.


## Other Notes & Information

N/A