# Meeting Minutes

**Week number**: 2


## Meeting Information

**Date/Time:** 28 September 2022, 2.15pm (postponed as planned)

**Purpose:** Weekly Meeting

**Location:** Winton Building Meeting Room C and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Specifications review | Comments and validation of the specifications document and its planning. |
| 3 | Literature review | Some papers have been found, but advices could be taken from AW. |
| 4 | Report details | Small details about the report shape. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. The task assigned to AW must be reported to next week. |
| 2 | LG | The document has been approved by AW and PB, but its content can be modified afterwards if necessary. Some details will be modified for next meeting. |
| 3 | All attendees | Advices from AW have been made in order to conduct a proper literature review. More information in the corresponding task. |
| 4 | LG | Questions about thesis report templating have been asked. Those details have been answered by the advisors. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 1 | AW | 05.10.2022 | Provide the coursework from last year to Loïc Guibert in order to explore the system architectures that have been explored and explained. |
| 2 | LG | 05.10.2022 | Update the specifications document and publish it on the forge. |
| 3 | LG | 05.10.2022 | Analyse the various possibilities of making systematic reviews and chose one. Look at which topic to discuss, which items must be defined, and exclusion criteria. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Wednesday, at room UBS008 on 1.30pm.


## Other Notes & Information

N/A