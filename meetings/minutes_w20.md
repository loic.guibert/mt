# Meeting Minutes

**Week number**: 20


## Meeting Information

**Date/Time:** 03 February 2023, 1.30pm

**Purpose:** Weekly Meeting

**Location:** UBS131 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Questions about the report | Some questions have to be asked on the report. |
| 3 | Thesis defence | Discussion about the date and time. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | Usage chapter: on the Web service context, remove ground truth, and refactor sentence. Change development team to Hestia developers. For the Section appraisal, change the title for evaluation. Rephrase the limitations as prospective / future work. On the abstract, give more concrete data on the results, and answer to the question on is this accurate? Plus, what if at the end of it. On the cover page: add the Winchester logo, add the "supported by school of...", and leave Adriana as supervisor, add a new line for Southampton. Appendix : add commits history, in the project management. |
| 3 | LG | The defence will be held the 1st of March at 3 PM (Fribourg time). |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 3 | LG | 03.02.2023 | Send an invitation for the defence. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

This meeting will normally be the last one. If held, next meeting will be schedule appropriately with the attendees' timetables.


## Other Notes & Information

N/A