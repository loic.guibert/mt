# Meeting Minutes

**Week number**: 8


## Meeting Information

**Date/Time:** 9 November 2022, 1.30pm

**Purpose:** Weekly Meeting

**Location:** UBS008 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Literature review | Comments and validation about the literature review. |
| 3 | Thesis title | Should we change the thesis title to be more specific? |
| 4 | Next steps | The *Methodology* activity will be started. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | The literature review is almost done, two topics need to be completed. Once finished, the chapter will be concluded with a summary. If additional knowledge will come up in the future, it will be completed. |
| 3 | LG | Acronym of the output? Based on the final result of the project, explain it in the title. Examples: provide a guide of security and privacy for... . Keywords: guidelines, rules, ... . Will talk about it later, when the output will be more precise. |
| 4 | LG | Methodology: assess other fields of IT, analyse / comment / criticize their methodology, and explain the difference between their approach and mine (adapt to my subject and specific angle). Ideas: start by listing the steps to be taken to have a secure and private system, then redirect to the chapters on each topic that explain how to implement. Output: 10 pages of synthesis of point to be checked to reach the goal, + additional explanation of the points with explanation. See to make rules (ex: data domain, always anonymize sensitive data if it goes public). |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 2 | LG | 11.11.2022 | Finish the review. |
| 4 | LG | 16.11.2022 | Start the *Methodology* activity and expose early findings. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Wednesday, at room UBS008 on 1.30pm.


## Other Notes & Information

N/A