# Meeting Minutes

**Week number**: 3


## Meeting Information

**Date/Time:** 5 October 2022, two sessions

**Purpose:** Weekly Meeting

**Location:** UBS008 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Specifications review | Comments and validation of the specifications document and its planning. |
| 3 | SOTA review | Comments and discussion about the methodology of the SOTA |
| 4 | Local talk | Organize the talk. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | No particular comment. |
| 3 | All attendees | The SOTA protocol has been discussed and is adapted, but small things have to be changed. Where to place research question, topics and related work (not a protocol, but result of the thinking process). Some definitions of web service, IT security, user privacy, etc must also be defined. |
| 4 | - | No discussion about that. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 1 | LG | 12.10.2022 | Finish the protocol and start the review. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Wednesday, at room UBS008 on 1.30pm.


## Other Notes & Information

N/A