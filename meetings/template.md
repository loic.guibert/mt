# Meeting Minutes

**Week number**: _


## Meeting Information

**Date/Time:** date, time

**Purpose:** Weekly Meeting

**Location:** UBS008 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- N/A

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Comments can be made. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
|  |  |  |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
|  |  |  |  |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
|  |  |  |


## Next meeting

The next meeting will be held next Wednesday, at the same place, same time.


## Other Notes & Information

N/A