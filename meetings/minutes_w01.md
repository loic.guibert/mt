# Meeting Minutes

**Week number**: 1


## Meeting Information

**Date/Time:** 13 September 2022, 1.30pm

**Purpose:** Weekly Meeting (Kick-off)

**Location:** UBS008 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde
- Loïc Guibert
- Pascal Bruegger

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Scope, aim of the thesis | Based on the broad scope described in initial project proposal, a more precise and defined direction must be decided for this thesis. This direction must be validated by both supervisors. |
| 2 | Specifications Document | Once the scope of the project has been validated, the specifications document must be written, This is a mandatory step of the thesis from the HES-SO Master School. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | All attendees | Some ideas and directions have been discussed in order to find the best approaches, objectives and directions for the thesis. What emerges is that the thesis will be focused on building a sort of framework that gives indications/indicators to IT actors on their online services' security and privacy for their users. A validation of this framework will then be applied to an existing system in order to evaluate this output. |
| 2 | Loïc Guibert | The document will follow the guidelines from the home school: the University of Winchester does not require this step. The next days will focus on a first draft on this document. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 2 | Loïc Guibert | 27.09.2022 | A draft of the specifications document must be sent before the next meeting. |
| - | Loïc Guibert | 28.09.2022 | If the specifications document is finished before the next meeting, a preliminary search on the literature review will be started. |
| - | Adriana Wilde | 28.09.2022 | Provide the coursework from last year to Loïc Guibert in order to explore the system architectures that have been explored and explained. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| 1 | All attendees | The project scope has been validated by all attendees. |


## Next meeting

The next meeting will be held next week, at the same place, same time.


## Other Notes & Information

Some remarks have been made about the whole thesis:
- Minimize uncertainness of the work (thesis / outcomes).
- Be sure what I am making is something new and not the continuity of a previous project, and if used, explain what come from those other projects.
- Do not forget to define the metrics in order to get a proper evaluation.

Also, once the specifications document validated, do not forget to change the Project Proposal Form content if attached to the thesis.