# Meeting Minutes

**Week number**: 19


## Meeting Information

**Date/Time:** 27 January 2023, postponed at 2.30pm

**Purpose:** Weekly Meeting

**Location:** UBS131 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Questions about the report | Some questions have to be asked on the report. |
| 3 | Next step | The next steps have to be discussed. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. No meetings have been planed last weeks, because the work to be done was defined, and no concerns had to be brought. The advisors agreed to that. |
| 2 | LG | Some parts of the report should be moved to other spaces. The improvements to be done on each chapter should be put in the corresponding summary, then summarized in the conclusion. |
| 3 | LG | First, the guide content will be finalized. Then, the test on a web service will be conducted. Finally, the report will be updated. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 3 | LG | 10.02.2023 | The next steps will be conducted. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Friday, at room UBS131 on 1.30pm.


## Other Notes & Information

N/A