# Meeting Minutes

**Week number**: 9


## Meeting Information

**Date/Time:** 16 November 2022, 1.30pm

**Purpose:** Weekly Meeting

**Location:** BoW131 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Evaluation format | Comments about this topic. |
| 3 | Review for evaluation methods | Comments about this topic. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | Explain the choices of the relevant approaches, then anaylse and compare them, could use a table for comparison. In another chapter, define my method then evaluate / compare it this the others. |
| 3 | LG | Not necessary to conduct a systematic review of the different methods. |

## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 2 & 3 | LG | - | Continue the work. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Wednesday, at room BoW131 on 1.30pm.


## Other Notes & Information

N/A