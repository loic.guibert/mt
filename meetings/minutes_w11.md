# Meeting Minutes

**Week number**: 11


## Meeting Information

**Date/Time:** 30 November 2022, 1.30pm

**Purpose:** Weekly Meeting

**Location:** UBS131 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Discussion about guide ideas |  |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | Critères d'évaluation pour guider l'user, avec un sous-titre (sous-section ou autre) pour un OBJECTIF qui comprend des points binaires à assess |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| - | - | - | - |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Wednesday, at room UBS131 on 1.30pm.


## Other Notes & Information

N/A