# Meeting Minutes

**Week number**: 4


## Meeting Information

**Date/Time:** 12 October 2022, 13 October 2022

**Purpose:** Weekly Meeting

**Location:** UBS008 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Literature review | Comments and validation about the literature review. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | Rework inclusion and exclusion criteria, be more specific and reproducible. Also, rework the tables to be more readable and comparable, and focus on the useful things for the guide, not for the whole topic. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 1 | LG | 09.11.2022 | Continue the review |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Wednesday, at room UBS008 on 1.30pm.


## Other Notes & Information

N/A