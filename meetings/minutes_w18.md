# Meeting Minutes

**Week number**: 18


## Meeting Information

**Date/Time:** 19 January 2023, 1.30pm

**Purpose:** Weekly Meeting

**Location:** UBS131 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Discuss the state of the thesis | The work done during the previous weeks will be presented and discussed. |
| 3 | Next steps | What should be done for next week: app testing, proposal testing on web app |
| 4 | Questions about report | How to deal with the annexes, small review from the supervisors, preface from PB, more details about the proposal. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. No meetings have been planed last weeks, because the work to be done was defined, and no concerns had to be brought. The advisors agreed to that. |
| 2 | LG | The work that has been done has been shown to the supervisors. What has been done has been validated. Some ideas have been shared to improve some parts of the application. |
| 3 | LG | First, some parts of the report's will be finished (some reshaping, introduction). Then, the spreadsheet containing the guide's content will be reviews to assess whether it is valid. Then, the guide will be tested on a web service. Then, the tests will be conducted. Finally, the report will be finished. |
| 4 | LG | Some parts of the report must be modified. The appendix section must be placed as the last part of the report, no preface will be done, and the supervisors agreed to read the report around week 5 if they have time. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 3 | LG | 10.02.2023 | The next steps will be conducted. |
| 4 | PB | 27.01.2023 | A meeting is planned to test the guide on a we b service. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | - | - |


## Next meeting

The next meeting will be held next Friday, at room UBS131 on 1.30pm.


## Other Notes & Information

N/A