# Meeting Minutes

**Week number**: 7


## Meeting Information

**Date/Time:** 2 November 2022, 3.30pm (postponed as planned)

**Purpose:** Weekly Meeting

**Location:** UBS008 and Microsoft Teams

**Note Taker:** Loïc Guibert


## People Involved

**Attendees:**
- Adriana Wilde (AW)
- Loïc Guibert (LG)
- Pascal Bruegger (PB)

**Excused:**
- N/A

**Missing:**
- N/A


## Agenda Items

ID | Item | Description |
| ---- | ---- | ---- |
| 1 | Validation of last minute | All attendees shall validate the meeting minute of last week. Various comments can be done. |
| 2 | Literature review | Comments and validation about the literature review. |
| 3 | PB venue | Details about PB coming at Winchester to give a talk. |


## Discussion Items

Item | Who | Notes |
---- | ---- | ---- |
| 1 | LG | The last meeting minute has been approved. |
| 2 | LG | The literature review conducting is going well, the general aspect of it is validated. However, the extracted content must be reworked: comparable items must remain in the tables, but standalone knowledge must be placed in the natural flow of the chapter. This task is not time-sensitive, as the content itself will stay the same. |
| 3 | All attendees | PB will come at Winchester from 7th December evening to the 11th December. A talk will be organized with students and professors. |


## Tasks

Item | Responsible | Due Date | Notes |
| ---- | ---- | ---- | ---- |
| 2 | LG | 09.11.2022 | Continue and finish the review. |
| 2 | LG | - | Adapt the SOTA chapter content. |
| 3 | All attendees | 30.11.2022 | Organize PB venue: define roadmap and abstract of the talk, let relevant people know about his venue. |


## Decisions

Item | Responsible | Notes |
| ---- | ---- | ---- |
| - | All attendees | PB will come at Winchester from 7th December evening to the 11th December. A talk will be organized with students and professors. |


## Next meeting

The next meeting will be held next Wednesday, at room UBS008 on 1.30pm.


## Other Notes & Information

N/A